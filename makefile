IDIR = "include"
CFLAGS = -I$(IDIR)

src = $(wildcard src/*.c) \
	  $(wildcard tests/*.c)
obj = $(src:.c=.o)
main := $(filter-out tests/test.o, $(obj))
test := $(filter-out src/main.o, $(obj))

#LDFLAGS = -lprintf 

myprog: $(main)
	$(CC) -o $@ $^ $(LDFLAGS) $(CFLAGS)

test: $(test)
	$(CC) -o $@ $^ $(LDFLAGS) $(CFLAGS)
	./test

.PHONY: clean
clean:
	rm -rf $(obj) test

